<?php

require 'syllables.inc.php';

function test($cword) {
  $syls = explode('~', $cword);
  $x = new Syllable(join('', $syls));
  $s = $x->getSyllables();
  if (assert($syls === $s)) return true;
  echo 'Expected: ' . join(' ~ ', $syls) . "\n";
  echo 'Got     : ' . join(' ~ ', $s) . "\n";
}

foreach (file('dic-test.txt', FILE_IGNORE_NEW_LINES|FILE_SKIP_EMPTY_LINES) as $word)
  test($word);
