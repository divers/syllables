<?php

mb_internal_encoding('UTF-8');

/*
    TODO: Accents, trémas, etc.
    * fléau
    * pays
*/

class Syllable {
  private $data;
  const v = 'ïëéèêàâùîaeiouy';
  const c = 'çbcdfghjklmnpqrstvwxz';

  function __construct($wordin = null) {
    if (is_null($wordin)) $this->data = array();
    else $this->init($wordin);
  }

  private function init($wordin) {
    // prevents infinite loop, no garantees regarding results
    $cnt = 50;
    $this->data = array();
    foreach ($this->specialCase($wordin) as $word) {
      while (--$cnt && $this->canCut($word)) {
        for ($r = 0; $r < mb_strlen($word) - 2; ++$r) {
          $l0 = mb_substr($word, $r, 1);
          $l1 = mb_substr($word, $r+1, 1);
          $l2 = mb_substr($word, $r+2, 1);
          if (mb_strlen($word) > $r+3 ) $l3 = mb_substr($word, $r+3, 1);
          if (mb_strlen($word) > $r+4 ) $l4 = mb_substr($word, $r+4, 1);
          // FIXME: Trop de redondance
          if ($this->isVowel($l0) && $this->isConsonant($l1) && $this->isVowel($l2)) {
              $this->data[] = mb_substr($word, 0, $r+1);
              $word = mb_substr($word, $r+1);
          } elseif ((mb_strlen($word) > 3) && $this->isVowel($l0)
            && $this->isConsonant($l1) && ($l1 === $l2)) {
              $this->data[] = mb_substr($word, 0, $r+2);
              $word = mb_substr($word, $r+2);
          } elseif ((mb_strlen($word) > 3) && $this->isVowel($l0)
            && $this->isConsonant($l1) && $this->isConsonant($l2) && $this->isVowel($l3)) {
              if (('g' === $l1) && ('n' === $l2)) {
                  $this->data[] = mb_substr($word, 0, $r+1);
                  $word = mb_substr($word, $r+1);
              } elseif (('n' === $l1)) {
                  $this->data[] = mb_substr($word, 0, $r+2);
                  $word = mb_substr($word, $r+2);
              } elseif (('h' === $l2)
                || (('s' === $l1) && ('c' === $l2))
                || (('r' !== $l1) && (('l' === $l2) || ('r' === $l2)))) {
                  $this->data[] = mb_substr($word, 0, $r+1);
                  $word = mb_substr($word, $r+1);
              } else {
                  $this->data[] = mb_substr($word, 0, $r+2);
                  $word = mb_substr($word, $r+2);
              }
          } elseif ((mb_strlen($word) > 4) && $this->isVowel($l0)
            && $this->isConsonant($l1) && $this->isConsonant($l2)
            && $this->isConsonant($l3) && $this->isVowel($l4)) {
              if (('h' === $l3)
                || (('r' !== $l2) && (('l' === $l3) || ('r' === $l3)))) {
                  $this->data[] = mb_substr($word, 0, $r+2);
                  $word = mb_substr($word, $r+2);
              } else {
                  $this->data[] = mb_substr($word, 0, $r+3);
                  $word = mb_substr($word, $r+3);
              }
          }
          else continue; // no hit? keep looping over $word with 'for'
          break; // we got a hit, stop 'for' loop over $word
        }//for
      }//while
      $this->data[] = $word;
    }
  }

  function getSyllables($word = null) {
    if (!is_null($word)) $this->init($word);
    return $this->data;
  }

  function __toString() {
    return join('~', $this->data);
  }

  private function canCut($word) {
    return preg_match('/[' . self::v . '][' . self::c . ']+[' . self::v . ']/u', $word);
  }

  private function isVowel($l) {
    return false !== mb_strpos(self::v, $l, 0, 'UTF-8');
  }

  private function isConsonant($l) {
    return false !== mb_strpos(self::c, $l);
  }

  private function specialCase($wordin) {
    // deals with 'ue' and 'ye' sequences
    @list($l, $r, $x) = explode('ue', $wordin, 3);
    if (!isset($x) && !empty($r) && ('s' !== $r)
      && ('i' !== mb_substr($r, 0, 1))
      && !(('q' === mb_substr($l, -1)) || ('g' === mb_substr($l, -1))))
        return array($l . 'u', 'e' . $r);

    @list($l, $r, $x) = explode('ye', $wordin, 3);
    return !isset($x) && !empty($r) && ('s' !== $r)
      && !(('q' === mb_substr($l, -1)) || ('g' === mb_substr($l, -1)))
        ? array($l . 'y', 'e' . $r)
        : array($wordin);
  }
}
